module.exports = {
	label: 'invite',
	generator: (bot, msg) => {
		const inviteLink =
			`https://discordapp.com/oauth2/authorize?&client_id=${bot.user.id}&scope=bot&permissions=523611216`;
		
		msg.author.getDMChannel().then(dmChannel => {
			dmChannel.createMessage({ embeds: [{
				color: 0xFFB2C5,
				description: `[Invite link](${inviteLink})`
			}]});
		});
	}
};
