const { getCodePoint } = require('../../util/StringUtil');

module.exports = {
	label: 'emoji',
	generator: (_, __, args) => {
		const validCustomEmoji = /^<:.+?:(\d+)>$/;

		const invalidArgs = [];

		args.forEach(e => {
			if (!validCustomEmoji.test(e))
				invalidArgs.push(e);
		});

		if (invalidArgs.length > 0) {
			if (invalidArgs.length === args.length)
				return invalidArgs.map(getCodePoint);

			// TODO: If `invalidArgs < args`, show only valids
			return 'There are some invalid inputs: ' + invalidArgs.join(' ');
		} else
			return args.reduce((acc, e) => {
				const i = e.replace(validCustomEmoji, '$1');
				return `${acc}\nhttps://cdn.discordapp.com/emojis/${i}.png`;
			});
	},
	options: {
		argsRequired: true,
		usage: `~emoji <server emoji/s>`
	}
};
