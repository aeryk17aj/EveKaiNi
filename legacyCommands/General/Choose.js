module.exports = {
	label: 'choose',
	generator: (_, __, args) => {
		const a = args.join(' ').split(/ ?\| ?/);
		return `I Pick... **${a[Math.floor(Math.random() * a.length)]}**`;
	},
	options: {
		aliases: [ 'pick' ],
		argsRequired: true,
		usage: 'Format: choose <choice1>|<choice2>|<...>'
	}
};
