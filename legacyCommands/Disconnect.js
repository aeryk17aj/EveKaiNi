const { botAuthor } = require('./_Permissions');
const { log } = require('../util/BotUtil');


const command = {
	label: 'dc',
	/**
	 * @arg {import('../core/Eve')} bot Client context
	 */
	generator: (bot) => {
		bot.sql.close().then(() => {
			bot.disconnect({ reconnect: false });
			log('[System] System Disconnected (command)');
			process.exit();
		});
	},
	/** @type {import('eris').CommandOptions} */
	options: {
		hidden: true,
		requirements: botAuthor
	}
};

module.exports = command;
