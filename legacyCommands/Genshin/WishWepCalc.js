const { wishWep } = require('./_WishUtil.js');

const usage = [
	'Calculates the amount of rolls and tells <...>.',
	'Does not store input, only calculates.',
	'Format: wishwep <primos>,[fates],[start pity]'
].join('\n');

module.exports = {
	label: 'wishwep',
	/**
	 * @arg {string[]} args
	*/
	generator: (_, msg, args) => {
		if (!args[0]) {
			return usage;
		}

		const [ primos, fates = 0, startPity = 0 ] = args[0].split(',').map(v => parseInt(v, 10));
		const { embeds } = wishWep(msg.author, primos, fates, startPity);

		msg.channel.createMessage({ embeds });
	},
	options: {
		aliases: ['ww'],
		usage
	}
};