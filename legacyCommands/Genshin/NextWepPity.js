const { wishWepData } = require('./_WishUtil.js');

const usage = [
	'Calculates an estimate of the amount of rolls in the value of primogems to get to the next pity.',
	'Supports Welkin with a `w` flag at the end (but not custom remaining days)',
	'Does not store input, only calculates.',
	'Format: nwp <primos>,[fates],[start pity] [w]'
].join('\n');

module.exports = {
	label: 'nextweppity',
	/**
	 * @arg {string[]} args
	*/
	generator: (_, msg, args) => {
		if (!args[0]) {
			return usage;
		}

		const [ primos, fates = 0, startPity = 0 ] = args[0].split(',').map(v => parseInt(v, 10));
		const welkin = args[1] != undefined ? args[1].toLowerCase() === 'w' : false;
		const welkinBonus = welkin ? 90 : 0;
		const {
			pityPreset,
			primoDraws, draws, totalPity,
			fiveMin, fiveMax
		} = wishWepData(primos, fates, startPity);

		const extraPrimos = primos % 160;

		let details = `:sparkles: ${draws}\nRolls: <:intertwinedFate:863310371791831051> ${fates}, <:primogem:863309545631907870> ${primoDraws}`;

		if (extraPrimos > 0) {
			details += ` (Extra <:primogem:863309545631907870> ${extraPrimos})`;
		}

		if (startPity > 0) {
			details += `\nCurrent total pity: ${totalPity}`;
		}

		if (welkin) {
			details += '\nWelkin: :white_check_mark:';
		}

		// Old method, inteded to count primos to soft after guarantees, was faulty past soft
		// const reqPrimosMin = (pityPreset.FiveSoft - totalPity % pityPreset.FiveHard) * 160 - extraPrimos;

		// How many primos to raise the fiveMax by 1
		const reqPrimosMin = ((fiveMax + 1) * pityPreset.FiveSoft - totalPity) * 160 - extraPrimos;
		
		// Old method similar to Min, but actually works the same as the new one below
		// const reqPrimosMax = (pityPreset.FiveHard - totalPity % pityPreset.FiveHard) * 160 - extraPrimos;

		// How many primos to to raise the fiveMin by 1
		const reqPrimosMax = ((fiveMin + 1) * pityPreset.FiveHard - totalPity) * 160 - extraPrimos;

		const reqRollsMin = (reqPrimosMin + extraPrimos) / 160;
		const reqRollsMax = (reqPrimosMax + extraPrimos) / 160;

		const daysMin = Math.ceil(reqPrimosMin / (60 + welkinBonus));
		const daysMax = Math.ceil(reqPrimosMax / (60 + welkinBonus));

		msg.channel.createMessage({ embeds: [{
			color: 0xFFB2C5,
			timestamp: new Date(Date.now()).toISOString(),
			footer: {
				text: `Calculated at`
			},
			author: {
				name: msg.author.username + '\'s next weapon pity',
				icon_url: msg.author.staticAvatarURL
			},
			fields: [
				{
					name: 'Details',
					value: details
				},
				{
					name: 'Next Pity',
					value: [
						`Soft: <:primogem:863309545631907870> ${reqPrimosMin} or more (\u2265${reqRollsMin} rolls, \u2265${daysMin} dailies)`,
						`Hard: <:primogem:863309545631907870> ${reqPrimosMax} or less (\u2264${reqRollsMax} rolls, \u2264${daysMax} dailies)`
					].join('\n')
				}
			]}]
		});
	},
	options: {
		aliases: ['nwp'],
		usage
	}
};