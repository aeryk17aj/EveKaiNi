const usage =
	'Usage: `~osu <subcommand>`\n' +
	'Subcommands:\n' + [
		'profile',
		'tvis',
	].join('\n');

module.exports = {
	label: 'osu',
	generator: usage,
	options: { usage }
};
