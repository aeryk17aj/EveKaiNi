const { commaPad, duration } = require('../../util/StringUtil');

const hitIds = ['300', '100', '50'];

/**
 * Shows user's game-specified performance, world rank, country rank, play count, and play time
 *
 * @arg {import('node-osu').User} user
 * @returns {string}
 */
function basicStats(user) {
	return [
		`${user.pp.raw} pp`,
		`:globe_with_meridians: #${commaPad(user.pp.rank)}`,
		`:flag_${user.country.toLowerCase()}: #${commaPad(user.pp.countryRank)}`,
		`Play count: ${commaPad(user.counts.plays)}`,
		`Play time: ${duration(user.secondsPlayed)} (${commaPad(user.secondsPlayed / 3600 | 0)}h)`
	].join('\n');
}

/**
 * Shows user's clear counts of the following plays:
 *
 * **SSH, SS, SH, S, A**
 *
 * Not exposed by the API:
 *
 * **B, C, D**
 *
 * @arg {import('node-osu').User} user
 * @returns {string}
 */
function clearCounts(user) {
	// TODO: custom emoji for clear rank
	return ['SS', 'S'].reduceRight((a, r) => {
		const count = commaPad(user.counts[r]);
		const countHidden = commaPad(user.counts[r + 'H']);
		return `${r}: ${count} | ${r}H: ${countHidden}\n${a}`;
	}, `A: ${commaPad(user.counts['A'])}`);
}

/**
 * Shows hit counts and rate of hitting
 *
 * @arg {import('node-osu').User} user
 * @arg {number} mode
 * @returns {string}
 */
function hits(user, mode = 0) {
	const totalHits = hitIds.reduce((a, b) => a + parseInt(user.counts[b], 10), 0);

	if (mode === 1) {
		hitIds.pop(); // no 50 in Taiko, or at least not counted
	}

	return hitIds
		.map((hitId) => {
			const rawHits = parseInt(user.counts[hitId], 10);
			const hits = commaPad(rawHits);
			const hitRate = ((rawHits / totalHits) * 100).toFixed(2);

			return `${hitId}s: ${hits} (${hitRate}%)`;
		})
		.join('\n');
}

/**
 * Shows weighted and unweighted accuracy
 *
 * Unweighted accuracy is calculated from hit stats
 *
 * @arg {import('node-osu').User} user
 * @arg {number} mode
 * @returns {string}
 */
function accuracies(user, mode = 0) {
	let unweighted = '';
	const totalHits = hitIds.reduce((a, b) => a + parseInt(user.counts[b], 10), 0);
	hitIds.forEach(a => user.counts[a] = parseInt(user.counts[a], 10));
	// console.log(`DEBUG HITS (MODE ${mode})\n${JSON.stringify(user.counts)}`);
	if (mode === 0) {
		const raw =
			(user.counts[300] + user.counts[100] / 3 + user.counts[50] / 6) /
			totalHits * 100;
		unweighted = raw.toFixed(2) + '%';
	} else if (mode === 1) {
		const raw = (user.counts[300] + user.counts[100] / 2) / totalHits * 100;
		unweighted = raw.toFixed(2) + '%';
	} else if (mode === 2) {
		// Correct weightings
		const raw =
			(user.counts[300] + user.counts[100] / 3 + user.counts[50] / 6) /
			totalHits * 100;
		unweighted = raw.toFixed(2) + '% :warning:';
	} else if (mode === 3) {
		const raw =
			(user.counts[300] + user.counts[100] / 3 + user.counts[50] / 6) /
			totalHits * 100;
		unweighted = raw.toFixed(2) + '% :warning:';
	} else {
		// TODO: Catch and Mania accuracy
		unweighted = 'WIP';
	}

	return [
		`Weighted: ${user.accuracyFormatted}`,
		`Unweighted: ${unweighted}`
	].join('\n');
}

module.exports = {
	basicStats,
	clearCounts,
	accuracies,
	hits
};
