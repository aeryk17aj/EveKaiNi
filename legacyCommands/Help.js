
/**
 * Returns the label and its aliases if available
 * @arg {import('eris').Command} cmd Command object
 * @returns {string}
 */
function getLabels (cmd) {
	let out = `${cmd.label}**`;
	if (cmd.options != undefined && cmd.options.aliases != undefined && cmd.options.aliases.length > 0)
		out += ['', ...cmd.options.aliases.map(l => `**${l}**`)].join('/');
		
	return out;
}


module.exports = {
	label: 'help',
	/**
	 * @arg {import('../core/Eve')} bot Client context to be used.
	 * @arg {import('eris').Message} msg
	 * @arg {string[]} args
	 */
	generator: (bot, msg, args) => {
		if (args.length === 0) {
			const fields = [];

			for (const sm of bot.rootModule.submodules) {
				fields.push({
					name: sm.name,
					value: sm.commands
						.filter(c => !c.hidden)
						.map(c => {
							let out = `**${msg.prefix}${getLabels(c)}`;
							const subCommandLabels = Object.values(c.subcommands)
								.map(sc => `**${getLabels(sc)}`);

							if (Object.keys(c.subcommands).length > 0)
								out += ` (${subCommandLabels.join(', ')})`;

							return out;
						})
						.join('\n')
				});
			}

			fields.push({
				name: 'Misc. Commands',
				value: bot.rootModule.commands
					.filter(c => !c.hidden)
					.map(c => `**${msg.prefix}${c.label}**`)
					.join('\n')
			});

			const embeds = [{
				color: 0xffb2c5,
				fields
			}];

			msg.channel.createMessage({ embeds });
		} else {
			const rc = args[0];
			const cmd = bot.commands[rc] ||
				bot.commands[bot.commandAliases[rc]]; // alias fallback
			let out = '';

			if (cmd == undefined)
				return 'Command does not exist.';

			if (rc !== cmd.label)
				out += `Alias: ${rc}\n`;

			if (args.length > 1) {
				let sc = cmd;
				// keep going down the subcommand tree until out of args
				for (const scl of args.slice(1)) {
					sc = cmd.subcommands[scl] ||
						cmd.subcommands[cmd.subcommandAliases[scl]]; // alias fallback
					if (sc == undefined)
						return 'Invalid subcommand label/alias: ' + scl;
				}
				out += sc.usage;
			} else {
				out += cmd.usage;
			}

			return out || 'No info yet. WIP';
		}
	}
};
