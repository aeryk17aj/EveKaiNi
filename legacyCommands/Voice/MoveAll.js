const { verifyMoveQuery } = require('./_voiceUtil');

module.exports = {
	label: 'moveUs',
	/**
	 * @arg {import('eris').Message} msg
	 * @arg {string[]} args
	 */
	generator: (_, msg, args) => {
		const { error, senderVc, destinationVc } = verifyMoveQuery(args.join(' '), msg);

		if (error)
			return error;
		else
			Promise.all(senderVc.voiceMembers.map(m => m.edit({ channelID: destinationVc.id })));
	},
	options: {
		argsRequired: true,
		usage: 'moveUs <vc name or id>'
	}
};
