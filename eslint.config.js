const babelParser = require('@babel/eslint-parser')
const js = require('@eslint/js')
const stylistic = require('@stylistic/eslint-plugin')
const globals = require('globals')
const jsdoc = require('eslint-plugin-jsdoc')

module.exports = [
  js.configs.recommended,
  stylistic.configs['disable-legacy'],
  stylistic.configs['recommended-flat'],
  {
    languageOptions: {
      globals: {
        ...globals.node,
        ...globals.es2021
      },
      parser: babelParser,
      sourceType: 'script'
    },
    plugins: {
      '@stylistic': stylistic
    },
    rules: {
      'no-const-assign': 1,
      'no-extra-boolean-cast': 0,
      'no-unused-vars': [1, {
        'argsIgnorePattern': '^_',
        'caughtErrors': 'none'
      }],
      'no-var': 1,
      'prefer-const': 1,
      'radix': 1,

      '@stylistic/array-bracket-spacing': 0,
      '@stylistic/arrow-parens': [1, 'as-needed'],
      '@stylistic/brace-style': [1, '1tbs'],
      '@stylistic/comma-dangle': [1, 'only-multiline'],
      '@stylistic/indent': [1, 'tab', {
        'SwitchCase': 1
      }],
      '@stylistic/no-multi-spaces': [1, {
        'ignoreEOLComments': true
      }],
      '@stylistic/no-tabs': 0,
      '@stylistic/no-trailing-spaces': [ 1, {
        'skipBlankLines': true,
        'ignoreComments': true
      }],
      '@stylistic/operator-linebreak': [1, 'none', {
        'overrides': {
          '=': 'after',
          '?': 'before',
          ':': 'before',
        }
      }],
      '@stylistic/quotes': [1, 'single'],
      '@stylistic/quote-props': [1, 'as-needed', {
        'unnecessary': false
      }],
      '@stylistic/semi': [1, 'always'],
      '@stylistic/space-before-function-paren': [1, 'always'],
      '@stylistic/spaced-comment': 1,
    },
  },
  {
    ...jsdoc.configs['flat/recommended'],
    rules: {
      ...jsdoc.configs['flat/recommended'].rules,
      'jsdoc/check-param-names': [1, {
        'disableMissingParamChecks': true
      }],
      'jsdoc/require-jsdoc': [1, {
        'exemptEmptyFunctions': true
      }],
      'jsdoc/require-param': [1, {
        contexts: [
          'Identifier[name=/^[^_]/]'
        ]
      }],
      'jsdoc/require-param-description': 0,
      'jsdoc/require-property-description': 0,
      'jsdoc/require-returns': [1, {
        'forceReturnsWithAsync': false
      }],
      'jsdoc/require-returns-description': 0,
      'jsdoc/tag-lines': [1, 'any', { 'startLines': 1 }]
    },
    settings: {
      jsdoc: {
        tagNamePreference: {
          'param': 'arg',
          'property': 'prop',
          'augments': 'extends',
        }
      }
    },
    ignores: [
      'commandInteractions/_util/*'
    ]
  },
  {
    files: [
      'eslint.config.js',
      '**/*.Test.js'
    ],
    rules: {
      '@stylistic/indent': [1, 2],
      '@stylistic/semi': [1, 'never']
    }
  },
  { ignores: [
    'legacyCommands/*',
    '*Test.js',
    '*/Elsword/BossFight.js',
    '*/Elsword/MiniEvent.js',
    '*/General/_parkourUtil.js',
    '*/General/En2Pa.js',
    '*/General/*Pa2En.js',
  ] }
]
