const fs = require('fs');

/** @typedef {fs.Dirent} Dirent */

/**
 * @arg  {...string} path 
 * @returns {Dirent[]}
 */
function getDirents (...path) {
	path = path || [];

	return fs.readdirSync(path.join('/'), { withFileTypes: true });
}

/**
 * @arg {...string} path
 * @returns {{ files: string[], folders: string[] }}
 */
function readModuleDir (...path) {
	const files = [];
	const folders = [];

	getDirents(...path).forEach(f => {
		if (f.name.startsWith('_'))
			return;

		if (f.isFile() && f.name.endsWith('.js'))
			files.push(f.name);
		else if (f.isDirectory())
			folders.push(f.name);
	});

	return { files, folders };
}

module.exports = {
	getDirents,
	readModuleDir
};
