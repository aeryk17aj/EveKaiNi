/**
 * @arg {string} s 
 */
function log (s) {
	process.stdout.write(s + '\n');
}

/**
 * Requires a dependency if it exists, otherwise null
 * 
 * @arg {string} name
 * @returns {any}
 */
function tryRequire (name) {
	try {
		require.resolve(name);
		return require(name);
	} catch (e) { /* NO OP */ }
	return null;
}

module.exports = {
	log,
	tryRequire
};
