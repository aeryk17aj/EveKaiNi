const { Constants } = require('eris');

module.exports = {
	name: 'ed2kc',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	scope: 'global',
	description: 'Convert ED to K-Ching with specified conversion rate',
	options: [{
		name: 'value',
		description: 'ED value in millions to be converted. Accepts shorthands of [k, m, b]',
		type: Constants.ApplicationCommandOptionTypes.STRING,
		required: true
	}, {
		name: 'ed-rate',
		description: 'Rate of x million ED per 100 KC',
		type: Constants.ApplicationCommandOptionTypes.NUMBER,
		min_value: 0.000001,
		required: true
	}]
};
