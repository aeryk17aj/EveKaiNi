const { Constants } = require('eris');

module.exports = {
	name: 'randomtown',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	description: 'Randomly picks an Elsword village or resting area',
	options: [{
		name: 'rest-area',
		description: 'Include Rest Areas (default false)',
		type: Constants.ApplicationCommandOptionTypes.BOOLEAN,
		required: false
	}]
};
