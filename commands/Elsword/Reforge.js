const { Constants } = require('eris');

module.exports = {
	name: 'reforge',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	description: 'Lists the materials to achieve specified Reforge Level',
	options: [{
		name: 'target',
		description: 'Target Reforge Level',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 1,
		max_value: 21,
		required: true
	}, {
		name: 'type',
		description: 'Optional, defaut Tenebrous. Armor Type',
		type: Constants.ApplicationCommandOptionTypes.STRING,
		choices: [{
			name: 'Rigomor',
			value: 'rigo'
		}, {
			name: 'Tenebrous',
			value: 'tene'
		}, {
			name: 'Exascale',
			value: 'exa'
		}],
		required: false
	}, {
		name: 'start',
		description: 'Optional, default target - 1. Starting Reforge Level',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		max_value: 20,
		required: false
	}, {
		name: 'starting-pity',
		description: 'Optional, default 0%. Current Reforge Pity %',
		type: Constants.ApplicationCommandOptionTypes.NUMBER,
		min_value: 0,
		max_value: 100,
		required: false,
	}, {
		name: 'alt-mat',
		description: 'Optional, default false. Use the alternate, tradeable material for the breakdown',
		type: Constants.ApplicationCommandOptionTypes.BOOLEAN,
		required: false
	}, {
		name: 'blacksmith',
		description: 'Optional, default 0. Blacksmith profession discount, if applicable',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		max_value: 10,
		required: false
	}, {
		name: 'ed-tix',
		description: 'Optional, default 0. Number of Reforge ED Fee Exemption Tickets',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 0,
		required: false
	}]
};
