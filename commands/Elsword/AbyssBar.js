const { Constants } = require('eris');

module.exports = {
	name: 'abyss-bar',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	scope: 'global',
	description: 'Calculates your real Abyss bar value.',
	options: [{
		name: 'percent',
		description: 'Bar percent value.',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 1,
		max_value: 100,
		required: true
	}]
};
