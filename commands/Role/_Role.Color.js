const { Constants: { ApplicationCommandOptionTypes: OptionTypes } } = require('eris');

module.exports = {
	name: 'color',
	description: 'Find or change your personal role color',
	options: [
		{
			name: 'new-color',
			description: 'New (hex) color of your personal role. Omit if not changing.',
			type: OptionTypes.STRING,
			max_length: 7
		}
	]
};
