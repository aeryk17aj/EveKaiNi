const { Constants: { ApplicationCommandOptionTypes: OptionTypes } } = require('eris');

module.exports = {
	name: 'name',
	description: 'Find or change your personal role name',
	options: [
		{
			name: 'new-name',
			description: 'New name of your personal role. Omit if not changing.',
			type: OptionTypes.STRING,
			max_length: 100
		}
	]
};
