const { Constants } = require('eris');

module.exports = {
	name: 'roll',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	scope: 'global',
	description: 'Generate a random number between 1 and the specified max (inclusive, default 100)',
	options: [{
		name: 'max',
		description: 'Maximum value (inclusive, default 100)',
		type: Constants.ApplicationCommandOptionTypes.INTEGER,
		min_value: 1,
		required: false,
	}]
};
