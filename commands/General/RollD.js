const { Constants } = require('eris');

module.exports = {
	name: 'rolld',
	type: Constants.ApplicationCommandTypes.CHAT_INPUT,
	scope: 'global',
	description:
		'Rolls a die or dice, depending on user input',
	options: [ {
		name: 'expr',
		description: 'Expression. Max: 100d1000. Examples: 3d6, 6d20',
		type: Constants.ApplicationCommandOptionTypes.STRING,
		required: true
	}]
};
