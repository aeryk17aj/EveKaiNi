`<>` - required<br>
`[]` - optional
# Commands
- **General**
	- `~choose <choice1> | <choice2> [ | <choice3>...]`
	- `~emoji <server emoji/s>`
	- `~invite`
	- `~ping`
	- `~roll [max]` - default max is 100
	- `~uptime`
- **Elsword**
	- `els`
		- `ed2kc <amount>[kmb] <ed>:<kc>` - can exclude suffix for raw value
		- `kc2ed <amount> <ed>:<kc>`
- **Granblue Fantasy**
	- `gbf`
		- `spark <crystals>,<oneTix>,<tenTix>`
- **osu!**
	- `osu`
		- `profile <username> [mode name]` - default is standard
		- `tvis [ dDkK]`
- **Voice**
	- `~moveMe <voice channel name>`
	- `~moveUs <voice channel name>` - might require sender to have vc move perms
- **Author only**
	- `~dc`
	- `~eval <code>`
  
# WIP Commands
- **General**
	- `~help` - output in progress
