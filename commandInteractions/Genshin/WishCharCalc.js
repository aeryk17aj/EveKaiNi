const { wishGenshinChar } = require('../_util/Pities.js');

module.exports = {
	name: 'wc',
	/**
	 * @arg {import('eris').CommandInteraction} interaction 
	 * @arg {{[n: string]: any}} args 
	 * @returns {Promise<void>}
	 */
	run: (_, interaction, args) => {
		const {
			primogems = 0,
			fates = 0,
			'starting-pity': startingPity = 0
		} = args;
		const { embeds } = wishGenshinChar(interaction.member.user, primogems, fates, startingPity);
		
		return interaction.createMessage({ embeds });
	}
};
