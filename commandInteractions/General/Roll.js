const { Constants } = require('eris');

function roll (max) {
	return Math.ceil(Math.random() * max);
}

// Function solely added to have components attached to it
function rollResult (result, max) {
	return {
		content: result,
		components: [{
			type: Constants.ComponentTypes.ACTION_ROW,
			components: [{
				type: Constants.ComponentTypes.BUTTON,
				style: Constants.ButtonStyles.PRIMARY,
				custom_id: `reroll ${max}`,
				label: 'Reroll'
			}, {
				type: Constants.ComponentTypes.BUTTON,
				style: Constants.ButtonStyles.SUCCESS,
				custom_id: 'clear-components',
				label: 'Settle'
			}]
		}]
	};
}

module.exports = {
	name: 'roll',
	/**
	 * @arg {import('eris').CommandInteraction} interaction 
	 * @arg {{ max: number }} args 
	 * @returns {Promise<void>}
	 */
	run: (_, interaction, args) => {
		const { max = 100 } = args;
		const result = roll(max);
		
		return interaction.createMessage(rollResult(result, max));
	},
	roll,
	rollResult
};
