module.exports = {
	name: 'kc2ed',
	run: ({ emojis }, interaction, args) => {
		const {
			value: amount,
			'ed-rate': edRate
		} = args;

		// not supporting m or b because it's unrealistic
		const realAmount = amount.endsWith('k')
			? parseFloat(amount.slice(0, -1), 10) * 1000
			: parseInt(amount, 10);

		const edValue = realAmount * edRate / 100;

		// in case raw kc arg is unsuffixed and >1k, to shorten output
		let inText = realAmount;
		if (realAmount >= 1000) {
			inText = (realAmount / 1000) + 'k';
		}

		let out = edValue + 'm';
		if (edValue >= 1000)
			out = (edValue / 1000) + 'b';
		else if (edValue < 1)
			out = (edValue * 1000) + 'k';

		interaction.createMessage(`${emojis.kc} **${inText}** at **${edRate}:100** is ${emojis.ed} **${out}**`);
	}
};
