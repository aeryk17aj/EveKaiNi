module.exports = {
	name: 'ed2kc',
	run: ({ emojis }, interaction, args) => {
		const {
			value: amount,
			'ed-rate': edRate
		} = args;

		const rawAmtArg = /^\d+$/.test(amount);
		let value;

		if (rawAmtArg) {
			value = parseInt(amount, 10);
		} else {
			const unit = amount[amount.length - 1];

			if (!['b', 'k', 'm'].includes(unit)) {
				interaction.createMessage({
					content: 'Invalid ED format, must be a value in millions ("9" -> 9m) or have a suffix of k, m, or b',
					flags: 64 // ephemeral
				});

				return;
			}

			const rawValue = parseFloat(amount.slice(0, -1));
			
			if (unit === 'b')
				value = rawValue * 1000;
			else if (unit === 'm')
				value = rawValue;
			else if (unit === 'k')
				value = rawValue / 1000;
		}

		// shorten in case raw, unsuffixed value to save space
		let shortValue = value + 'm';
		if (value >= 1000) {
			shortValue = (value / 1000) + 'b';
		} else if (value < 1) {
			shortValue = (value * 1000) + 'k';
		}

		const out = Math.ceil(value / edRate * 100).toLocaleString();
		interaction.createMessage(`${emojis.ed} **${shortValue}** at **${edRate}:100** is ${emojis.kc} **${out}**`);
	}
};
