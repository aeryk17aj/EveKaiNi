module.exports = {
	name: 'abyss-bar',
	run: (_, interaction, args) => {
		const { percent } = args;

		const fullBar = 84;
		const pctToRaw = p => Math.ceil(p / 100 * fullBar);
		const pctReal = r => Math.round(r * (100 / fullBar) * 100) / 100;
		const barString = r => `${r}/${fullBar} (${pctReal(r)}%)`;

		const raw = pctToRaw(percent);
		const nextRaw = pctToRaw(percent + 1);

		let out = '';

		if (raw !== nextRaw) {
			out = barString(raw);
		} else {
			const prvRaw = pctToRaw(percent - 1);
			out += `${barString(prvRaw)},  ${barString(nextRaw)}\n`;
			out += `-# In-game value is always rounded down. ${percent}% is impossible.`;
		}

		interaction.createMessage(out);
	}
};
