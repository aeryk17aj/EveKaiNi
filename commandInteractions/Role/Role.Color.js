const { Constants } = require('eris');
const { preCheck, canEditRole } = require('./_RoleUtil');

module.exports = {
	name: 'myrole color',
	/**
	 * @arg {import('eris').CommandInteraction<import('eris').TextChannel>} interaction 
	 * @arg {{'new-color': string}} args
	 * @returns {Promise<void>}
	 */
	run: async (bot, interaction, args) => {
		/** @returns {Promise<string>} */
		async function changeRoleColor () {
			const pr = preCheck(interaction);
			if (pr.canManage || !pr.hasSoloRole) return pr.message;
			
			const newColor = args['new-color'];
			if (!newColor)
				return '#' + ('0000' + pr.soloRole.color.toString(16)).slice(-6);
			
			let colorStr = newColor;
			const validColor = /^#?((?:[\dA-F]{3}){1,2})$/i;
			if (!validColor.test(colorStr))
				return `Invalid color format: '${colorStr}', use hex.`;
			if (colorStr.length === 3)
				// expand 3 digit
				colorStr = colorStr.replace(/([\dA-Fa-f])/g, '$1$1');
	
			const color = parseInt(colorStr.match(validColor)[1], 16);
			if (pr.soloRole.color === color)
				return 'Same color. No change made.';
			
			if (canEditRole(bot, pr.soloRole)) {
				await pr.soloRole.edit({ color }, 'Color change command');
				return 'Role color changed.';
			} else {
				return 'Insufficient permission to modify your personal role.';
			}
		}

		return interaction.createMessage({
			content: await changeRoleColor(),
			flags: Constants.MessageFlags.EPHEMERAL
		});
	}
};
