const { Constants } = require('eris');

/**
 * @arg {CommandInteraction} interaction
 * @arg {boolean} shouldHaveRole
 * @returns {PreCheckReturn}
 */
function preCheck (interaction, shouldHaveRole = false) {
	const out = {
		canManage: false,
		hasSoloRole: false,
	};

	const exp = message => ({ ...out, message });

	if (!interaction.appPermissions.has(Constants.Permissions.manageRoles))
		return exp('No **Manage Roles** permission.');

	out.canManage = true;
	const soloRole = getSoloRole(interaction.member);
	if (!soloRole && shouldHaveRole)
		return exp('No personal role');
	else {
		out.hasSoloRole = true;
		return {
			...out,
			message: `You already have a solo color role: ${soloRole.name}`,
			soloRole: soloRole
		};
	}
}

/**
 * @typedef {object} PreCheckReturn
 * @prop {boolean} canManage
 * @prop {boolean} hasSoloRole
 * @prop {string} message
 * @prop {Role?} soloRole
 */

/**
 * @arg {Member} member
 * @returns {Role?}
 */
function getSoloRole (member) {
	let minPos = -1;
	let outRole = null;
	for (const roleId of member.roles) {
		const roleMembers = member.guild.members.filter(m => m.roles.includes(roleId));

		if (roleMembers.length === 1) {
			const role = member.guild.roles.get(roleId);
			if (!(role.hoist && role.mentionable)) {
				// TODO: If mentionable/hoisted, ask again?
				if (role.position > minPos) {
					minPos = role.position;
					outRole = role;
				}
			}
		}
	}
	return outRole;
}

/**
 * @arg {Eve} bot
 * @arg {Guild} guild
 * @returns {number}
 */
function getBotTopRolePos (bot, guild) {
	const { members, roles } = guild;
	const botMemRoles = members.get(bot.user.id).roles
		.map(roleId => roles.get(roleId).position);
	return Math.max(...botMemRoles);
}

/**
 * Finds the highest possible position to put a role in to apply the color to a specific member
 * 
 * @arg {Eve} bot
 * @arg {Member} member
 * @returns {number}
 */
function findColorableRolePos (bot, member) {
	// Get member's roles in order or descending position
	const memRoles = member.roles
		.map(roleId => member.guild.roles.get(roleId))
		.sort((a, b) => b.position - a.position);

	const maxPos = getBotTopRolePos(bot, member.guild);

	for (const role of memRoles) {
		if (role.position < maxPos)
			return role.position;
	}
	
	return 0; // bottom, right above @everyone
}

/**
 * @arg {Eve} bot
 * @arg {import('eris').Role} role 
 * @returns {boolean}
 */
function canEditRole (bot, role) {
	return getBotTopRolePos(bot, role.guild) > role.position;
}

/**
 * @typedef {import('../../core/Eve.js')} Eve
 * @typedef {import('eris')} Eris
 * @typedef {import('eris').CommandInteraction<import('eris').TextChannel>} CommandInteraction
 * @typedef {import('eris').Guild} Guild
 * @typedef {import('eris').Member} Member
 * @typedef {import('eris').Role} Role
 */

module.exports = {
	preCheck,
	getSoloRole,
	findColorableRolePos,
	canEditRole
};
