/**
 * @arg {import('./Sparks').SparkPreset} preset 
 * @arg {number} crystals 
 * @arg {number} oneTix 
 * @arg {number} tenTix 
 * @arg {number} sparks
 */
function processDraws (preset, crystals = 0, oneTix = 0, tenTix = 0, sparks = 0) {
	const gemDraws = Math.floor(crystals / preset.gemDraw);
	const draws = gemDraws + oneTix + (tenTix * 10);
	
	const totalDraws = draws + sparks;
	
	const srPlus = Math.floor((draws - oneTix) / 10);
	
	return {
		preset, crystals, oneTix, tenTix, sparks,
		
		gemDraws, draws, totalDraws,
		
		srPlus
	};
}

/**
 * @arg {object} root0
 * @arg {import('./Sparks').SparkPreset} root0.preset
 * @arg {number} root0.crystals
 * @arg {number} root0.oneTix
 * @arg {number} root0.tenTix
 * @arg {number} root0.sparks
 * @arg {number} root0.gemDraws
 * @arg {number} root0.draws
 * @arg {number} root0.totalDraws
 * @arg {number} root0.srPlus
 * @arg {Member|User} author 
 * @returns {{ embed: Embed }}
 */
function toEmbed ({
	preset, crystals, oneTix, tenTix, sparks,
	gemDraws, draws, totalDraws,
	srPlus
}, author) {
	const drawBreakdown = sparks > 0 ? `${totalDraws} (**${draws}** + ${sparks})` : draws;
	
	const srPlusE = `${preset.emojis.sr}/${preset.emojis.ssr}`;
	
	let out = `:sparkles: ${drawBreakdown} *(${srPlus} ${srPlusE})*\n`;
	
	if (totalDraws >= preset.spark) {
		out += '**Spark possible!**';
		
		const sparks = totalDraws / preset.spark;
		if (sparks >= 2) {
			out += ` (${sparks | 0}x)`;
		}
	} else {
		out += 'Spark not possible.';
	}
	
	const color = totalDraws >= preset.spark ? 0x00FF00 : 0xFF0000;
	
	const fields = [
		{
			name: 'Total draws',
			value: out
		},
		{
			name: `${preset.emojis.gem} ${crystals}`,
			value: `:sparkles: ${gemDraws}\n${srPlusE} ${srPlus - tenTix}`,
			inline: true
		}
	];
	
	if (oneTix > 0) {
		fields.push({
			name: `${preset.emojis.oneTix} 1T: ${oneTix}`,
			value: `:sparkles: ${oneTix}`,
			inline: true
		});
	}
	
	if (tenTix > 0) {
		fields.push({
			name: `${preset.emojis.tenTix} 10T: ${tenTix}`,
			value: `:sparkles: ${tenTix * 10}\n${srPlusE} ${tenTix}`,
			inline: true
		});
	}
	
	const embeds = [{
		color,
		timestamp: new Date(Date.now()).toISOString(),
		footer: {
			text: 'Calculated at'
		},
		author: {
			name: author.username + '\'s spark',
			icon_url: author.staticAvatarURL
		},
		fields
	}];
	
	return { embeds };
}

/**
 * @typedef {import('eris').Member} Member
 * @typedef {import('eris').User} User
 * @typedef {import('eris').Embed} Embed
 */

module.exports = { processDraws, toEmbed };
