const { processDraws, toEmbed } = require('./SparkUtil.js');
const emojis = require('../../core/emojis.json');

// TODO: Add game terms to presets: gem, oneTix

/** @type {SparkPreset} */
const PresetGBF = {
	spark: 300,
	gemDraw: 300,
	emojis: {
		gem: emojis.crystal || '<:crystal:749069663224463430>',
		oneTix: '<:1t:749070256076619797>',
		tenTix: '<:10t:749070255971762227>',
		sr: '<:sr:749072149339308192>',
		ssr: '<:ssr:749072149209284629>'
	},
	names: {
		gem: 'Crystal',
		oneTix: 'Single Draw Ticket',
		tenTix: 'Ten Draw Ticket'
	}
};

/** @type {SparkPreset} */
const PresetBA = {
	spark: 200,
	gemDraw: 120,
	emojis: {
		gem: emojis.pyroxene,
		oneTix: emojis.ba1t || '<:ba1t:1091934828389220403>',
		tenTix: emojis.ba10t || '<:ba10t:1091934846001090611>',
		sr: emojis.ba2s || '<:ba2s:1092229317091659898>', // 2*
		ssr: emojis.ba3s || '<:ba3s:1092229319176237056>' // 3*
	},
	names: {
		gem: 'Pyroxene',
		oneTix: '1-Recruitment Ticket',
		tenTix: '10-Recruitment Ticket'
	}
	
};

// The new way
const _PresetMap = {
	GBF: PresetGBF,
	// PC: PresetPC,
	BA: PresetBA
};

/**
 * @arg {string} presetKey
 */
function spark (presetKey) {
	/**
	 * @arg {number} crystals
	 * @arg {number} oneTix
	 * @arg {number} tenTix
	 * @arg {number} sparks
	 */
	return function /* step1 */ (crystals, oneTix, tenTix, sparks) {
		const out = processDraws(_PresetMap[presetKey], crystals, oneTix, tenTix, sparks);
		return {
			data: out,
			/**
			 * @param {string} author 
			 * @returns {Object}
			 */
			embed: function /* step2 */ (author) {
				return toEmbed(out, author);
			}
		};
	};
}

/**
 * @name SparkPresetStringMap
 * @prop {string} gem
 * @prop {string} oneTix
 * @prop {string} tenTix
 */
/**
 * @typedef SparkPreset
 * @prop {number} spark
 * @prop {number} gemDraw
 * @prop {SparkPresetStringMap} emojis
 * @prop {string} emojis.sr
 * @prop {string} emojis.ssr
 * @prop {SparkPresetStringMap} names
 */

module.exports = {
	spark
};
