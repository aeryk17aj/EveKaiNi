/**
 * @arg {import('./Pities').PityPreset} preset
 * @arg {number} gems
 * @arg {number} oneTix
 * @arg {number} startPity
 */
function processDraws (preset, gems, oneTix = 0, startPity = 0) {
	const gemDraws = Math.floor(gems / preset.gemDraw);
	const draws = gemDraws + oneTix;
	const totalPity = draws + startPity;

	const fiveMin = Math.floor(totalPity / preset.FiveHard) * (draws > 0);
	const fiveMax = Math.floor(totalPity / preset.FiveSoft) * (draws > 0);
	// 5* pulls can offset 4* pity
	// Pitfall: assumes totalPity % 10 is on zero 4* pity, which is only 1/10 of the time
	// It corrects itself to match below methods by just subtracting 1 on both, idk why yet
	// const fourMin = Math.floor((draws + (totalPity % 10) - fiveMax) / pityPreset.FourHard);
	// const fourMax = Math.floor((draws + (totalPity % 10) - fiveMin) / pityPreset.FourSoft);

	// TODO: Simplification
	const totalPityFourMin = Math.floor((totalPity - fiveMax) / preset.FourHard);
	const startPityFourMin = Math.floor(startPity / preset.FourHard);

	const totalPityFourMax = (totalPity - fiveMin) / preset.FourSoft;
	const startPityFourMax = startPity / preset.FourSoft;

	const fourMin = totalPityFourMin - startPityFourMin;
	const fourMax = Math.ceil(totalPityFourMax - startPityFourMax);

	return {
		// Args
		preset, gems, oneTix, startPity,

		// Calculated draws and pity
		gemDraws, draws, totalPity,

		// Calculated 4*s and 5*s
		fiveMin, fiveMax,
		fourMin, fourMax,

		// Extra
		fiveSoft: fiveMax > fiveMin,
		fourSoft: fourMax > fourMin
	};
}

function toEmbed ({
	preset, gems, oneTix, startPity,
	gemDraws, draws, totalPity,
	fiveMin, fiveMax,
	fourMin, fourMax
}, author) {
	// Output string start
	let out = `:sparkles: ${draws} (`;

	if (fourMin < fourMax) {
		out += `${fourMin}-`;
	}

	out += `${fourMax}x :purple_circle: | `;

	if (fiveMin < fiveMax) {
		out += `${fiveMin}-`;
	}

	out += `${fiveMax}x :yellow_circle:)\n`;
	// Output string end

	if (startPity > 0) {
		out += `Starting Pity: ${startPity} draws\n`;
	}

	if (totalPity < preset.FiveHard && totalPity >= preset.FiveSoft) {
		out += '**Pity possible**\n';
	} else if (totalPity < preset.FiveSoft) {
		out += 'Pity not possible.\n';
	}

	out += `Total Pity: ${totalPity}`;

	let color = 0xFF0000; // Pity < Soft
	if (totalPity >= preset.FiveHard)
		color = 0x00FF00; // Pity >= Hard
	else if (totalPity >= preset.FiveSoft)
		color = 0xFFFF00; // Soft <= Pity < Hard

	const fields = [{
		name: 'Total draws',
		value: out
	}];

	if (gems > preset.gemDraw && oneTix > 0) {
		fields.push({
			name: 'Breakdown',
			value: [
				`${preset.emojis.gem} ${gems} = :sparkles: ${gemDraws}`,
				`${preset.emojis.oneTix} ${oneTix} = :sparkles: ${oneTix}`
			].join('\n'),
			inline: true
		});
	}

	const embeds = [{
		color,
		timestamp: new Date(Date.now()).toISOString(),
		footer: {
			text: 'Calculated at'
		},
		author: {
			// TODO: Less arbitrary way to tell if char or wep
			name: `${author.username}'s ${preset.FiveHard === 90 ? 'character' : 'weapon'} wishes`,
			icon_url: author.staticAvatarURL
		},
		fields
	}];

	return { embeds };
}

module.exports = { processDraws, toEmbed };
