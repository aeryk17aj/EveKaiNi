const { processDraws, toEmbed } = require('./PityUtil.js');

// Following this infographic
// https://twitter.com/genshin_wishes/status/1409659194681163781

const PresetGenshinChar = {
	gemDraw: 160,
	FourSoft: 9,
	FourHard: 10,
	FiveSoft: 74,
	FiveHard: 90,
	emojis: {
		gem: '<:primogem:863309545631907870>',
		oneTix: '<:intertwinedFate:863310371791831051>'
	}
};

const PresetGenshinWep = {
	gemDraw: 160,
	FourSoft: 8,
	FourHard: 9,
	FiveSoft: 63,
	FiveHard: 77,
	emojis: {
		gem: '<:primogem:863309545631907870>',
		oneTix: '<:intertwinedFate:863310371791831051>'
	}
};

// TODO: HI3

function wishGenshinCharData (primos, fates = 0, startPity = 0) {
	return processDraws(PresetGenshinChar, primos, fates, startPity);
}

function wishGenshinChar (author, primos, fates = 0, startPity = 0) {
	return toEmbed(wishGenshinCharData(primos, fates, startPity), author);
}

function wishGenshinWepData (primos, fates = 0, startPity = 0) {
	return processDraws(PresetGenshinWep, primos, fates, startPity);
}

function wishGenshinWep (author, primos, fates = 0, startPity = 0) {
	return toEmbed(wishGenshinWepData(primos, fates, startPity), author);
}

module.exports = {
	wishGenshinCharData, wishGenshinChar,
	wishGenshinWepData, wishGenshinWep
};
