const { roll, rollResult } = require('../../commandInteractions/General/Roll.js');

module.exports = {
	name: 'reroll',
	/**
	 * @arg {import('eris').ComponentInteraction} interaction 
	 * @arg {string[]} args 
	 * @returns {Promise<void>}
	 */
	run: (_, interaction, args) => {
		const max = parseInt(args[0], 10);
		
		return interaction.editParent(rollResult(roll(max), max));
	}
};
