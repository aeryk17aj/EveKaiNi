const fsu = require('../../util/FSUtil.js');

const LegacyCommandModule = require('./LegacyCommandModule.js');

// Naming scheme: `<Command>[.<SubComand>].js`

/**
 * @arg {LegacyCommandModule} [cmdModule]
 * @arg {...string} modulePath
 */
function processCommandModules (cmdModule, ...modulePath) {
	const { files, folders } = fsu.readModuleDir(...modulePath);
	const currentPath = modulePath.join('/');

	// Initialize submodules as deep as possible while there are folders found
	if (folders.length > 0) {
		folders.forEach((f, i) => {
			cmdModule.submodules.push(new LegacyCommandModule(f));
			processCommandModules(
				cmdModule.submodules[i],
				...[...modulePath, f]
			);
		});
	}

	// Once the folder processing is done, add commands to the respective module
	if (files.length > 0)
		processCommands(cmdModule, ...files.map(f => `${currentPath}/${f}`));
}

/**
 * @arg {LegacyCommandModule} m
 * @arg {...string} files
 */
function processCommands (m, ...files) {
	const moduleCommands = files.filter(f => /\/([A-Za-z0-9]+)\.js/.test(f));
	// TODO: Deeper subcommand support, => change regexes to match (don't literal, capture name ^)
	const subCommandLabels = files.filter(f => /([A-Za-z0-9]+\.){2,}js/.test(f));

	const subCommands = {};

	if (subCommandLabels.length) {
		// Subcommand map of commands to subcommand array
		moduleCommands.forEach(mc => subCommands[mc] = []);

		subCommandLabels.forEach(sc => {
			const { commandLabel, subcommand } = processSubcommand(moduleCommands, sc);
			subCommands[commandLabel].push(subcommand);
		});
	}

	moduleCommands.forEach(r => registerCommand(r, m, subCommands[r] || []));
}

/**
 * @arg {string} path
 * @arg {LegacyCommandModule} m
 * @arg {import('eris').Command[]} subcommands
 */
function registerCommand (path, m, subcommands) {
	const { label, generator, options } = require(`../../${path}`);
	m.addCommand(label, generator, options, subcommands);
}

/**
 * @arg {string[]} mcls Module Command Labels
 * @arg {string} scl Subcommand Label
 */
function processSubcommand (mcls, scl) {
	for (const mcl of mcls) {
		if (scl.startsWith(mcl.slice(0, -3))) {
			const subcommand = require(`../../${scl}`);
			return { commandLabel: mcl, subcommand };
		}
	}
}

module.exports = {
	processCommandModules
};
