/**
 * @template T
 *
 * @class BackgroundTask
 */
class BackgroundTask {
	/**
	 * Creates an instance of BackgroundTask.
	 * 
	 * @arg {() => T} init
	 * @arg {(t: T) => void} close
	 * @memberof BackgroundTask
	 */
	constructor (init, close) {
		this.ref = init();
		this.close = () => {
			close(this.ref);
		};
	}
}

module.exports = BackgroundTask;
