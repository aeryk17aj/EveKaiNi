const fs = require('fs');
const path = require('path');
const { readdir } = fs.promises;

const Eris = require('eris');
require('./extension/Extensions')(Eris);
const { Sequelize } = require('sequelize');

const BackgroundTask = require('./background/BackgroundTask');
const LegacyCommandModule = require('./command/LegacyCommandModule.js');
const LegacyCommandRegistry = require('./command/LegacyCommandRegistry.js');
const { readModuleDir } = require('../util/FSUtil.js');

const { log } = require('../util/BotUtil');

/**
 * @typedef {{[name: string]: string | number | boolean}} ModuleConfig
 * @typedef {object} EveConfig
 * @prop {string} commandRoot
 * @prop {string} handlerRoot
 * @prop {string} commandInteractions
 * @prop {string} componentInteractions
 * @prop {string} legacyCommands
 * @prop {{[name: string]: ModuleConfig}} modules
 */

/**
 * @class Eve
 * @extends {Eris.CommandClient}
 */
class Eve extends Eris.CommandClient {
	/**
	 * @arg {{ token: string, sql: object }} param0
	 * @arg {import('eris').ClientOptions} [options]
	 * @arg {import('eris').CommandClientOptions} [commandOptions]
	 */
	constructor ({ token, sql }, options, commandOptions) {
		super(token, options, commandOptions);

		/** @type {{[name: string]: BackgroundTask}} */
		this.backgroundTasks = {};

		this.setupSqlClient(sql);

		this.log = log;

		/** @type {EveConfig} */
		this.config = {
			bot: {},
			modules: {}
		};

		/** @type {Eris.ChatInputApplicationCommandStructure[]} */
		this.globalCommands = [];

		/** @type {Eris.ChatInputApplicationCommandStructure[]} */
		this.guildCommands = [];

		/** @type {import('./emojis.json')} */
		this.emojis = {};

		/** @type {{[name: string]: Promise<*>}}} */
		this.commandInteractions = {};

		/** @type {{[name: string]: Promise<*>}}} */
		this.componentInteractions = {};
	}

	connect () {
		this.initConfig();
		this.bindHandlers();
		this.registerLegacyCommands();
		this.initCommands();
		this.registerCommandInteractions();
		this.registerComponentInteractions();
		super.connect();
	}

	initConfig () {
		// enforced config root
		const configRoot = `${process.cwd()}/config/`;

		if (!fs.existsSync(configRoot)) {
			fs.mkdirSync(configRoot);
		}

		const configExists = fs.existsSync(`${process.cwd()}/config/config.json`);

		// TODO: config doc
		if (!configExists) { // Make default config
			fs.writeFileSync(
				`${process.cwd()}/config/config.json`,
				JSON.stringify({
					roots: {
						commands: 'commands',
						handlers: 'handlers',
						commandInteractions: 'commandInteractions',
						componentInteractions: 'componentInteractions',
						legacyCommands: 'legacyCommands'
					}
				}, null, 4)
			);
		}

		this.config.bot = require(configRoot + 'config.json');

		readdir(configRoot, { withFileTypes: true }).then(dirents => {
			const moduleConfigs = dirents.filter(dirent => {
				const [name, ext] = dirent.name.split('.');
				return dirent.isFile() && ext === 'json' && name !== 'config';
			});

			moduleConfigs.forEach(dirent => {
				const moduleName = dirent.name.split('.')[0];
				this.config.modules[moduleName] = require(configRoot + dirent.name);
			});
		}).then(() => {
			if (!configExists)
				this.saveConfig();
		});
	}

	bindHandlers () {
		const handlerRoot = `${process.cwd()}/${this.config.bot.roots.handlers || 'handlers'}/`;

		readdir(handlerRoot).then(files => {
			if (files.length === 0)
				return; // Skip if there's no event handlers.

			files.forEach(file => {
				const e = require(handlerRoot + file);
				if (e.name)
					this.on(e.name, (...a) => e.f(this, ...a));
			});
		}, this.log);
	}

	registerLegacyCommands () {
		const cmdRootDir = this.config.bot.roots.legacyCommands || 'legacyCommands';

		// Check if directory exists
		if (!fs.existsSync(`${process.cwd()}/${cmdRootDir}`))
			return;
		// dir exists, but empty
		else if (fs.readdirSync(`${process.cwd()}/${cmdRootDir}`).length === 0)
			return;

		// If called on refresh, this resets the module
		this.rootModule = new LegacyCommandModule('root');

		// Organizes commands to a root command module
		LegacyCommandRegistry.processCommandModules(this.rootModule, cmdRootDir);

		// Registers the commands all at once to the bot
		this.rootModule.addToBot(this);
	}

	/**
	 * Regenerates and re-registers commands
	 */
	refreshLegacyCommands () {
		for (const label of Object.keys(this.commands))
			this.unregisterCommand(label);

		this.registerLegacyCommands();
	}

	/**
	 * Recursively searches every matching file
	 * and pushes into defaultGuildCommands
	 *
	 * @arg {...any} path Root commands folder path
	 */
	#pushCommands (...path) {
		const root = path.join('/');
		const { files, folders } = readModuleDir(root);

		if (folders.length > 0) {
			folders.forEach(f => this.#pushCommands(root, f));
		}

		if (files.length > 0) {
			files.forEach(f => {
				const { scope, ...cmd } = require(`${process.cwd()}/${root}/${f}`);

				if (scope != undefined && scope === 'global')
					this.globalCommands.push(cmd);
				else
					this.guildCommands.push(cmd);
			});
		}
	}

	initCommands () {
		const cmdsRoot = this.config.bot.roots.commands;
		this.#pushCommands(cmdsRoot);
	}

	/**
	 * Recursively searches every matching file
	 * and pushes into a specified interaction map
	 * 
	 * @arg {{[name: string]: Promise<*>}} interactionMap
	 * @arg {...string} path Root interactions folder path
	 */
	#pushInteractions (interactionMap, ...path) {
		const root = path.join('/');
		const { files, folders } = readModuleDir(root);

		if (folders.length > 0) {
			folders.forEach(f => this.#pushInteractions(interactionMap, root, f));
		}

		if (files.length > 0) {
			files.forEach(f => {
				const response = require(`${process.cwd()}/${root}/${f}`);
				console.log(`Adding response for '${response.name}'`);
				interactionMap[response.name] = response;
			});
		}
	}

	registerCommandInteractions () {
		const cmdIntersRoot = this.config.bot.roots.commandInteractions;
		this.#pushInteractions(this.commandInteractions, cmdIntersRoot);
	}

	registerComponentInteractions () {
		const cmptIntersRoot = this.config.bot.roots.componentInteractions;
		this.#pushInteractions(this.componentInteractions, cmptIntersRoot);
	}

	refreshCommands () {
		this.globalCommands = [];
		this.guildCommands = [];
		this.initCommands();

		this.commandInteractions = {};
		this.registerCommandInteractions();

		this.componentInteractions = {};
		this.registerComponentInteractions();

		this.bulkEditCommands(this.globalCommands);

		this.guilds.forEach(g => {
			this.bulkEditGuildCommands(g.id, this.guildCommands);
		});
	}

	setupSqlClient ({ host, database, username, password }) {
		this.sql = new Sequelize({
			dialect: 'postgres',
			host,
			database,
			username,
			password,
			define: {
				timestamps: false,
				freezeTableNames: true
			}
		});

		this.testSqlConnection();
	}

	testSqlConnection () {
		this.sql.authenticate()
			.then(() => this.log('SQL Client Success'))
			.catch(err => this.log('SQL Client Error\n' + JSON.stringify(err, null, 4)));
	}

	/**
	 * Retrieves the bot's member object from a specifieed guild
	 * Current use case: Role system
	 *
	 * @arg {import('eris').Guild} guild
	 * @returns {import('eris').Member}
	 * @memberof Eve
	 */
	memberOf (guild) {
		return this.guilds.get(guild.id).members.get(this.user.id);
	}

	/**
	 * @arg {string} name
	 * @returns {ModuleConfig}
	 */
	/* getModuleConfig (name) {
		return this.config.modules[name];
	} */

	saveRootConfig () {
		fs.writeFileSync(
			path.resolve('./config/config.json'),
			JSON.stringify(this.config.bot, null, 4),
			'utf-8'
		);
	}

	saveModuleConfig (mdl, f = 'config') {
		fs.writeFileSync(
			path.resolve(`./config/${mdl}/${f}.json`),
			JSON.stringify(this.config.modules[mdl], null, 4),
			'utf-8'
		);
	}

	saveConfig () {
		this.saveRootConfig();
		for (const mdl in this.config.modules) {
			this.saveModuleConfig(mdl);
		}
	}

	/**
	 * @template T
	 * @arg {string} name
	 * @arg {() => T} init
	 * @arg {(t: T) => void} close
	 * @memberof Eve
	 */
	addBackgroundTask (name, init, close) {
		this.backgroundTasks[name] = new BackgroundTask(init, close);
	}

	/**
	 * Closes then deletes a background task
	 * 
	 * @arg {string} name
	 * @memberof Eve
	 */
	removeBackgroundTask (name) {
		this.backgroundTasks[name].close();
		delete this.backgroundTasks[name];
	}
}

module.exports = Eve;
