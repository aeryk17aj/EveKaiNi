const fs = require('node:fs');

const { log } = require('../util/BotUtil');

/** @type {Array<{ name: string, type: number }>} */
// const statuses = require('./statuses');

module.exports = {
	name: 'ready',
	/**
	 * @arg {import('../core/Eve')} bot
	 */
	f: async bot => {
		const timestamp = new Date(bot.startTime);
		const date = timestamp.toLocaleDateString('en-US');
		const time = timestamp.toLocaleTimeString('en-US', { hour12: true });
		log(`[${date} ${time}] [Startup] Connected as ${bot.user.username}`);
		
		/*
		0 Playing ___
		1 Streaming ___ (don't use)
		2 Listening to ___
		3 Watching ___
		4 <emoji> ___
		5 Competing in ___
		*/

		/* bot.addBackgroundTask('status', () => {
			const changeStatus = () => {
				const [ text, type ] = statuses[Math.floor(Math.random() * statuses.length)];
				if (type === 4)
					bot.editStatus({ name: 'custom', state: text, type });
				else
					bot.editStatus({ name: text, type });
			};

			changeStatus();

			const minute = 60_000; // in ms
			const i = setInterval(changeStatus, 10 * minute);
			i.unref();
			return i;
		}, i => clearInterval(i)); */
		
		bot.getEmojis().then(({ items }) => {
			if (items.length > 0) {
				items.forEach(i => {
					bot.emojis[i.name] = `<:${i.name}:${i.id}>`;
				});

				const emojisFile = `${process.cwd()}/core/emojis.json`;
				if (fs.existsSync(emojisFile)) {
					const emojis = require(emojisFile);
					const emojiNames = Object.keys(emojis);
					if (emojiNames.length === items.length) {
						let foundMismatch = false;
						emojiNames.forEach(k => {
							if (emojis[k] !== bot.emojis[k])
								foundMismatch = true;
						});
						if (!foundMismatch)
							return;
					}
				}
				
				fs.writeFileSync(emojisFile, JSON.stringify(bot.emojis, null, 2));
			}
		});

		await bot.bulkEditCommands(bot.globalCommands);

		bot.guilds.forEach(g => {
			bot.bulkEditGuildCommands(g.id, bot.guildCommands);
			// delete per-guild if guild config has commands in blacklist
		});
	}
};
